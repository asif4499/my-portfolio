
$(document).ready(function(){
      $(window).scroll(function(){
        var scroll = $(window).scrollTop();
          if (scroll > 300) {
            $("#scroll").css({
                "background": "#202340",
                "transition": ".8s",
                "box-shadow": "0 4px 8px 0 rgba(0, 0, 0, 0.40), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
            });
          }

          else{
              $("#scroll").css({
                  "background" : "",
                  "box-shadow" : ""
              });  	
          }
      })
    
    
    /*popover*/
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
    
    /*scrollspy*/
      $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
              // Prevent default anchor click behavior
              event.preventDefault();

              // Store hash
              var hash = this.hash;

              // Using jQuery's animate() method to add smooth page scroll
              // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
              $('html, body').animate({
                scrollTop: $(hash).offset().top
              }, 1200, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
              });
            } // End if
          });
    
    /*isotope*/
    $('.portfolio-item').isotope({
      itemSelector: '.item',
      layoutMode: 'fitRows'
    });
    $('.portfolio-menu ul li').click(function(){
        $('.portfolio-menu ul li').removeClass('active');
        $(this).addClass('active');
        
        var selector = $(this).attr('data-filter');
        $('.portfolio-item').isotope({
            filter: selector
        });
        return false;
    })
    
     /*wow js*/
	new WOW().init();
    
    
    /*counter-up*/
       $('.counter').counterUp({
            delay: 20,
            time: 3000,
            /*offset: 70,*/
            formatter: function (n) {
            return n.replace(/,/g, '.');
        }
    });
    
    
    
})
