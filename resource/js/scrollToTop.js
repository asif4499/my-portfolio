    /*=================================================
      scroll to top 
=======================================================*/
    $('.scrolltotop').fadeOut(1000);
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 700) {
            $('.scrolltotop').fadeIn(1000);
        } else {
            $('.scrolltotop').fadeOut(1000);
        }
    });
    $('.scrolltotop').on('click', function () {
        $('html,body').animate({
            scrollTop: 0
        }, 1000);
    });